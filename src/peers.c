/*!
 * \file
 * \brief handles connections to the P2P network
 */

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// TODO: REMOVE
#include <stdio.h>

#include "peers.h"
#include "types.h"

/*!
 * \brief create a peer object. Will not connect it to the network
 * \param peerAddr IPv4 address for this peer. Should be in host endianness
 * \param peerPort port for this peer. Should be in host endianness
 * \param thePeer pointer to peer which will be set
 */
void createPeer(uint32_t peerAddr, unsigned int peerPort, peer *thePeer) {
    // clear peer for good measure
    // (note: this isn't actually just for good measure, it's required)
    memset(thePeer, 0, sizeof(peer));
    struct sockaddr_in peerAddrObj = {
        .sin_family = AF_INET,
        .sin_port = htons(peerPort),
        .sin_addr = peerAddr,
    };
    thePeer->sockid = socket(AF_INET, SOCK_STREAM, 0);
    SET_SOCKET_NON_BLOCKING(thePeer->sockid);
    thePeer->status = peerNotConnected;
    thePeer->addr = peerAddrObj;
}

/*!
 * \brief connect a peer object to the network
 * establishes a TCP connection to the peer
 * \param thePeer peer to connect. Addr, port, etc are already set from `createPeer`
 * \return 0 for success, 1 for started, -1 for failure
 */
static int connectPeer(peer *thePeer) {
    if(!(thePeer->status == peerNotConnected || thePeer->status == peerConnecting)) {
        // peer is already connected
        return 0;
    }
    int connectRes = connect(thePeer->sockid, (struct sockaddr *)(&(thePeer->addr)), sizeof(struct sockaddr));
    // if successful
    if(!connectRes) {
        // allocate recv and send buffers
        thePeer->recvBuffer = malloc(1024 * 16);
        thePeer->sendBuffer = malloc(1024 * 16);
        thePeer->status = peerSendingVersion;
        return 0;
    }
    thePeer->status = peerConnecting;
    // EINPROGRESS is for initial connect, EALREADY is if you try again after that but still not done
    if(errno == EINPROGRESS || errno == EALREADY) {
        return 1;
    }
    printf("Connect failed! Err code: %d\n", errno);
    return -1;
}

/*!
 * \brief disconnect a peer and free memory used by it
 * \param thePeer peer to disconnect
 */
static void disconnectPeer(peer *thePeer) {
    CLOSE_SOCKET(thePeer->sockid);
    thePeer->status = peerNotConnected;
    thePeer->recvBufferLength = 0;
    thePeer->sendBufferLength = 0;
    free(thePeer->recvBuffer);
    free(thePeer->sendBuffer);
}

#define SERIALIZED_VERSION_LENGTH 26

/*!
 * \brief serialize the version message
 * \param addr ipv4 address of the peer, big endian
 * \param port port of the peer, big endian
 * \param bytes where result will be placed. Probably will need at least 26 bytes allocated, but maybe more in future if user agent is implemented
 */
static void serializeVersion(uint32_t addr, uint16_t port, unsigned char *bytes) {
    // version, we don't need anything newer than 60001
    serializeUInt32(60001, bytes);
    bytes += 4;
    // services, we don't provide any
    serializeUInt64(0, bytes);
    bytes += 8;
    // timestamp
    serializeUInt64(time(NULL), bytes);
    bytes += 8;
    // addr_recv, currently ipv4 only
    // TODO: ipv6
    // services subfield
    serializeUInt64(0, bytes);
    bytes += 8;
    // empty part before ipv4 address for ipv6 stuff
    memset(bytes, 0, 12);
    bytes += 12;
    // ipv4 address
    memcpy(bytes, &addr, 4);
    bytes += 4;
    // port
    memcpy(bytes, &port, 2);
    bytes += 2;
    // addr_from
    // services subfield
    serializeUInt64(0, bytes);
    bytes += 8;
    // ip address, ::ffff::127.0.0.1 since we don't know, care, or want people to connect to us
    memset(bytes, 0, 6);
    bytes += 6;
    bytes[0] = 0xff;
    bytes[1] = 0xff;
    bytes[2] = 127;
    bytes[3] = 0;
    bytes[4] = 0;
    bytes[5] = 1;
    bytes += 6;
    // port, 0 i guess?
    bytes[0] = 0;
    bytes[1] = 0;
    bytes += 2;
    // nonce, can just be 0 to ignore it (I don't think we'll connect to ourself, and it doesn't matter much)
    serializeUInt64(0, bytes);
    bytes += 8;
    // user agent string
    // TODO: var length strings and user agent
    bytes[0] = 0;
    bytes += 1;
    // starting height
    // TODO: correct starting height
    bytes[0] = 0;
    bytes += 1;
}

/*!
 * \brief parse the response from a version message and make sense out of it.
 * \param thePeer peer which this message was received from
 * \param msg actual version message
 * \return whether connection to this peer should continue
 */
static bool checkVersionResponse(peer *thePeer, const peerMessage *msg) {
    if(msg->payloadLength < 26) {
        return false;
    }
    // all we need to check are version and timestamp, currently
    // so, we also don't care about user agent length or anything
    thePeer->version = deserializeUInt32(msg->payload);
    // below 60001 don't support ping/pong properly, and it's 5 years old anyways
    if(thePeer->version < 60001) {
        return false;
    }
    thePeer->timeOffset = deserializeUInt32(msg->payload) - time(NULL);
    return true;
}

//! finds the number of peers connected or currently connecting within a pool
static int findConnectedPeerCount(peerPool *pool) {
    int toReturn = 0;
    for(int i = 0; i < pool->peerCount; ++i) {
        toReturn += pool->peers[i].status != peerNotConnected;
    }
    return toReturn;
}

/*!
 * \brief connect to the network/create a pool,
 * Will read peers from disk, connect the best ones,
 * run the looper every loop, handle peer everything,
 * yaeh this is pretty big.
 * The looper function should be `bool looper(void *state, peerPool *pool, peerMessage *newMsgs)`.
 * It should return whether or not the loop should continue. newMsgs are the new peer messages since the last loop.
 * 
 * \param looper function which is run every time something gets updated.
 */
void startPool(stateFn looper) {
    // LOAD PEERS
    // for now, this just means this preloaded list of peers
    // TODO: actually read from disk
    # define PRESET_PEER_COUNT 23
    // these came from seed.bitcoin.jonasschnelli.ch
    // dig +short seed.bitcoin.jonasschnelli.ch | sed -r 's/.*/"&",/g'
    int curAddrIndex = 0;
    // 15 is max length of an ip address, +1 for null
    char presetPeerAddrStrs[PRESET_PEER_COUNT][16] = {
        "5.29.125.106",
        "5.167.240.35",
        "89.233.160.174",
        "86.138.226.149",
        "210.186.242.68",
        "5.19.163.38",
        "88.99.184.81",
        "78.60.109.137",
        "93.156.84.136",
        "212.254.188.233",
        "173.177.51.22",
        "93.76.48.113",
        "89.114.228.24",
        "14.2.1.225",
        "1.171.226.218",
        "177.40.54.128",
        "1.171.195.52",
        "5.3.158.65",
        "18.85.54.190",
        "24.19.245.62",
        "108.31.111.130",
        "77.52.122.12",
        "91.46.238.128",
    };
    peer presetPeers[PRESET_PEER_COUNT];
    uint32_t curAddr;
    for(int i = 0; i < PRESET_PEER_COUNT; ++i) {
        inet_pton(AF_INET, presetPeerAddrStrs[i], &curAddr);
        createPeer(curAddr, 8333, &presetPeers[i]);
    }
    peerPool pool = {
        .peerCount = PRESET_PEER_COUNT,
        .peers = presetPeers,
    };
    // MAIN LOOP
    /*
     * The way the main loop works is like this:
     * first, we have the "pre-body" stuff, which runs after each poll() has completed.
     * This does recv()s and other things which can be done instantly, if possible.
     * It also calls handlers if a command is found.
     * Then, we have the "body", which is where the main logic is supposed to happen.
     * We run the user-defined loop function and break if everything is complete
     * Additionally, we also establish extra connections if necessary, sort of like a non-user-defined loop function
     * Then, we have "post-body" stuff, where we send(), connect(), and start other async operations.
     */
    struct pollfd pollfds[LBH_MAX_CONNECTIONS];
    peer *pollfdPeers[LBH_MAX_CONNECTIONS];
    int pollfdsCount = 0;
    while(true) {
        // PRE-BODY
        for(int i = 0; i < pollfdsCount; ++i) {
            if(pollfds[i].revents && pollfdPeers[i]->status == peerConnecting) {
                int connectRes = connectPeer(pollfdPeers[i]);
                if(connectRes < 0) {
                    printf("Finishing connect error! %d\n", errno);
                } else if(connectRes == 0) {
                    puts("Peer connection successful! Starting version stuff...");
                    // TODO: clean up how the version stuff is sent
                    // currently, connectPeer() doesn't actually send version stuff, but DOES set peer status -- this is a bit weird
                    // TODO: maybe have a dedicated function for sending stuff to peers?
                    pollfdPeers[i]->sendBufferLength = SERIALIZED_VERSION_LENGTH;
                    serializeVersion(pollfdPeers[i]->addr.sin_addr.s_addr, pollfdPeers[i]->addr.sin_port, pollfdPeers[i]->sendBuffer);
                } else {
                    puts("Peer connection returned 1, idk man");
                }
                continue;
            }
            if(pollfds[i].revents & POLLIN) {
                // TODO: avoid potential buffer overflow error because recvBuffer is hardcoded at 16kib!
                // there's probably somewhere where send buffer also needs a fix
                int recvRes = recv(pollfdPeers[i]->sockid, pollfdPeers[i]->recvBuffer, 1024, 0);
                if(recvRes < 0) {
                    printf("recv error! %d\n", errno);
                } else {
                    pollfdPeers[i]->recvBufferLength += recvRes;
                    // TODO: remove
                    if(pollfdPeers[i]->recvBufferLength > 15000) {
                        puts("recv buffer almost full!");
                    }
                    // check if we have a complete message
                    // we do 2 checks to short circuit and avoid a segfault in the check length function
                    if(pollfdPeers[i]->recvBufferLength > PEER_MESSAGE_HEADER_LENGTH && findPeerMessageLength(pollfdPeers[i]->recvBuffer) >= pollfdPeers[i]->recvBufferLength) {
                        peerMessage *pm;
                        bool pmRes = deserializePeerMessage(pollfdPeers[i]->recvBuffer, pm);
                        if(!pmRes) {
                            puts("Peer message deserialization failed!");
                        } else {
                            printf("Command received: %s\n", pm->command);
                            pollfdPeers[i]->recvBufferLength -= pm->payloadLength;
                        }
                    }
                }
            }
            if(pollfds[i].revents & POLLOUT) {
                puts("writable!");
                if(pollfdPeers[i]->sendBufferLength > 0) {
                    int bytesSent = send(pollfdPeers[i]->sockid, pollfdPeers[i]->sendBuffer, pollfdPeers[i]->sendBufferLength, 0);
                    if(bytesSent < 0) {
                        printf("send error! %d\n", errno);
                    } else {
                        pollfdPeers[i]->sendBufferLength -= bytesSent;
                    }
                }
            }
        }
        // BODY
        // call the looper
        // oh god this is ugly. Cast the function pointer to the correct type, dereference, then call
        if(!(*((bool (*)(void *, peerPool *))(looper.fn)))(looper.state, &pool)) {
            // we should disconnect now, so do it!
            for(int i = 0; i < pool.peerCount; ++i) {
                if(pool.peers[i].status != peerNotConnected) {
                    disconnectPeer(&pool.peers[i]);
                }
            }
            // disconnect is probably complete now
            break;
        }
        // Connect to more peers if we don't have enough
        if(findConnectedPeerCount(&pool) < LBH_MAX_CONNECTIONS) {
            // TODO: use a smarter algorithm for detecting the best peers to connect to
            // currently, connect to a random not-currently-connected peer, closest to a random number!
            for(int k = 0; k < LBH_MAX_CONNECTIONS - findConnectedPeerCount(&pool); ++k) {
                int targetRand = rand() % pool.peerCount;
                int closestDistance = 9999;
                peer *closestPeer = NULL;
                for(int i = 0; i < pool.peerCount; ++i) {
                    if(pool.peers[i].status == peerNotConnected && abs(i - targetRand) < closestDistance) {
                        closestDistance = abs(i - targetRand);
                        closestPeer = &pool.peers[i];
                    }
                }
                if(closestPeer == NULL) {
                    // nobody to connect to!
                    puts("WARNING: no available peers to connect to");
                    break;
                }
                // let's connect!
                int connectRes = connectPeer(closestPeer);
                puts("Attempted peer connection...");
                // TODO: better error checking, here and pretty much everywhere else
                if(connectRes < 0) {
                    puts("WARNING: Initial connect error!");
                }
            }
        }
        // POST-BODY
        // set pollfds based on peers
        pollfdsCount = 0;
        for(int i = 0; i < pool.peerCount; ++i) {
            if(pool.peers[i].status != peerNotConnected) {
                // add them to the list for reads
                pollfds[pollfdsCount].fd = pool.peers[i].sockid;
                pollfds[pollfdsCount].events = POLLIN;
                pollfds[pollfdsCount].revents = 0;
                pollfdPeers[pollfdsCount] = &pool.peers[i];
                // if there is data waiting to be sent or a connection is in progress, listen for writability too
                if(pool.peers[i].status == peerConnecting || pool.peers[i].sendBufferLength > 0) {
                    // i love this operator
                    pollfds[pollfdsCount].events |= POLLOUT;
                }
                ++pollfdsCount;
            }
        }
        // poll! 1 second timeout
        POLL(pollfds, pollfdsCount, 1000);
    }
    puts("Loop complete, everything is done, go fuck yourself");
}