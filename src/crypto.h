#ifndef LBH_CRYPTO_H
#define LBH_CRYPTO_H

// we use this instead of OpenSSL's SHA256_DIGEST_LENGTH because we might remove openssl in the future
#define SHA256_DIGEST_LENGTH 32

#include <stddef.h>
#include <openssl/sha.h>

#include "types.h"

// TODO: replace this at some point when we dump OpenSSL
#define sha256d(toHash, toHashLength, digest) SHA256(SHA256(toHash, toHashLength, NULL), 32, digest)
// unsigned char *sha256d(const unsigned char *toHash, const size_t toHashLength, unsigned char *digest);

void calculateMerkleRoot(const merkleBranch *branch, const uint256 hash, uint256 root);

#endif
