/*!
 * \file crypto.c
 * \brief Contains cryptographic stuff
 *
 * sha256d and merkle trees
 */

#include <string.h>
#include <stdbool.h>

#include "crypto.h"
#include "types.h"
#include "test-inject.h"

/*!
 * \brief calculate the merkle root for a merkle branch
 *
 * \param branch the merkle branch
 * \param hash the hash at the "bottom" of the tree
 * \param root the root will go here
 */
void calculateMerkleRoot(const merkleBranch *branch, const uint256 hash, uint256 root) {
    // root is used to store the current single hash
    // workingMemory is used to store two hashes consecutively so that they can be hashed into the "next-level" hash
    unsigned char workingMemory[SHA256_DIGEST_LENGTH * 2];
    memcpy(root, hash, SHA256_DIGEST_LENGTH);
    for(int i = 0; i < branch->depth; ++i) {
        // get the current bit out of the mask
        bool complementOnLeft = (branch->sideMask >> i) & 1 == 1;
        // copy current hash to the correct spot
        memcpy(workingMemory + (complementOnLeft ? SHA256_DIGEST_LENGTH : 0), root, SHA256_DIGEST_LENGTH);
        // and copy the complement
        memcpy(workingMemory + (complementOnLeft ? 0 : SHA256_DIGEST_LENGTH), branch->complements[i], SHA256_DIGEST_LENGTH);
        // and do the hashing
        sha256d(workingMemory, SHA256_DIGEST_LENGTH * 2, root);
    }
}
