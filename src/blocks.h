#ifndef LBH_BLOCKS_H
#define LBH_BLOCKS_H

#include <stdint.h>
#include <stdbool.h>
#include "types.h"

void deserializeHeader(const unsigned char *serialized, blockHeader *deserialized);
void serializeHeader(unsigned char *serialized, const blockHeader *deserialized);
bool verifyHeader(const blockHeader *verifyMe, const blockContext *context, headerRejectReason *rejectReason);

#endif
