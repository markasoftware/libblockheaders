/*!
 * \file
 * \brief Contains data structures and functions to deal with them
 */

#ifndef LBH_TYPES_H
#define LBH_TYPES_H

#include <stdint.h>
#include <stdbool.h>

typedef unsigned char uint256[32];

/*!
 * \brief a single block header
 * all fields are described on the Bitcoin Protocol page on the bitcoin.it wiki
 */
typedef struct {
    uint32_t version;
    uint256 previousBlock;
    uint256 merkleRoot;
    uint32_t timestamp;
    uint32_t target;
    uint32_t nonce;
    uint256 hash;
} blockHeader;

/*!
 * \brief a merkle branch
 * all fields are described on the Merged Mining Specification page on the bitcoin.it wiki,
 * but we name them a bit differently, so some description is provided here too.
 */
typedef struct {
    unsigned int depth; //<! how many complements there are (how "deep" the tree is)
    uint256 complements[32]; //<! the hashes which should be pre/appended to the "main" hash
    uint32_t sideMask; //<! see the spec
} merkleBranch;

/*!
 * \brief a "context" passed to verifyHeader to tell it about surrounding blocks
 * \sa verifyHeader
 */
typedef struct {
    uint256 previousBlock; //!< hash of block before one being verified
    uint32_t medianTimestamp; //!< median timestamp of last 11 blocks
    uint32_t networkTimestamp; //!< current network timestamp
    uint32_t target; //!< target for current difficulty period
} blockContext;

/*!
 * \brief all the different reasons a block can be rejected during validation
 * \sa verifyHeader
 */
typedef enum {
    rejectTargetNotEqual, //!< target in the block was not equal to the target in the context
    rejectPreviousBlockNotEqual, //!< previous block hash not equal to the target in context
    rejectTimestampBehind, //!< timestamp is older than median of last 11 blocks
    rejectTimestampAhead, //!< timestamp is too far in the future (usually >2hrs)
    rejectPoW, //!< block hash did not meet the proof-of-work target
} headerRejectReason;

//! a function with some state that will be passed to it
typedef struct {
    void *state; //!< state which will be passed as the first argument to the function
    void (*fn)(); //!< function which will be called with `state` as its first argument
} stateFn;

// TODO: move this into peers.[ch]
/*!
 * \brief a message that can be sent to a peer over the p2p network
 */
typedef struct {
    char command[12]; //!< ascii string of the command
    unsigned long payloadLength; //!< length of payload in bytes
    unsigned char *payload; //!< payload bytes
} peerMessage;

#define PEER_MESSAGE_HEADER_LENGTH 24

void serializePeerMessage(unsigned char *serialized, const peerMessage *deserialized);
bool deserializePeerMessage(const unsigned char *serialized, peerMessage *deserialized);
size_t findPeerMessageLength(const unsigned char *serialized);

#define deserializeUInt16(bytes) deserializeUInt(bytes, 2)
#define serializeUInt16(num, bytes) serializeUInt(bytes, num, 2)
#define deserializeUInt32(bytes) deserializeUInt(bytes, 4)
#define serializeUInt32(num, bytes) serializeUInt(bytes, num, 4)
#define deserializeUInt64(bytes) deserializeUInt(bytes, 8)
#define serializeUInt64(num, bytes) serializeUInt(bytes, num, 8)

unsigned long long deserializeUInt(const unsigned char *bytes, int length);
void serializeUInt(unsigned char *bytes, unsigned long long num, int length);

unsigned int deserializeVarLengthInt(const unsigned char *bytes, uint64_t *num);
unsigned int serializeVarLengthInt(unsigned char *bytes, uint64_t num);

#endif