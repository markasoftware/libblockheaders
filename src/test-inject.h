/*!
 * \file
 * \brief Used to allow easy mocking of functions
 * Functions which will be mocked are defined here, and the function bodies are in tests/test-inject.c
 * We use ld's --wrap option to allow us to override the "real" function. Cool, right?
 */

#ifndef LBH_TEST_INJECT_H
#define LBH_TEST_INJECT_H

void *__wrap_malloc(int numBytes);
void __wrap_free(void *segment);

#endif
