#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "types.h"
#include "crypto.h"

// we do all the bitshifting stuff manually because we can't be sure whether it's natively little endian or not
// potentially, we could determine endianness of the system, and, if it's little endian, just return a pointer
// to the same bytes/num just casted to a different thing and only swap if big endian

/*!
 * \brief convert up to 9 bytes into a var length integer

 * \param bytes bytes to convert
 * \param num how many bytes the number turned out to be (set by function)
 * \return the number of bytes parsed
 */
unsigned int deserializeVarLengthInt(const unsigned char *bytes, uint64_t *num) {
    if(bytes[0] < 0xfd) {
        *num = (uint64_t)bytes[0];
        return 1;
    }
    if(bytes[0] == 0xfd) {
        *num = ((uint64_t)bytes[2] << 8) + (uint64_t)bytes[1];
        return 3;
    }
    if(bytes[0] == 0xfe) {
        *num = ((uint64_t)bytes[4] << 24) + ((uint64_t)bytes[3] << 16) + ((uint64_t)bytes[2] << 8) + (uint64_t)bytes[1];
        return 5;
    }
    *num = ((uint64_t)bytes[8] << 56) + ((uint64_t)bytes[7] << 48) + ((uint64_t)bytes[6] << 40) + ((uint64_t)bytes[5] << 32) + ((uint64_t)bytes[4] << 24) + ((uint64_t)bytes[3] << 16) + ((uint64_t)bytes[2] << 8) + (uint64_t)bytes[1];
    return 9;
}

/*!
 * \brief convert a uint64_t into an up-to-9-byte var length integer
 *    for(int i = branch->depth; i > 0; ++i) {

 * \param bytes where the result will be placed, recommended to place at least 9 bytes
 * \param num the number to convert
 * \return how many bytes the result is
 */
unsigned int serializeVarLengthInt(unsigned char *bytes, uint64_t num) {
    if(num < 0xfd) {
        bytes[0] = num;
        return 1;
    }

    if(num <= 0xffff) {
        bytes[0] = 0xfd;
        bytes[1] = num & 0xff;
        bytes[2] = (num >> 8) & 0xff;
        return 3;
    }

    if(num <= 0xffffffff) {
        bytes[0] = 0xfe;
        bytes[1] = num & 0xff;
        bytes[2] = (num >> 8) & 0xff;
        bytes[3] = (num >> 16) & 0xff;
        bytes[4] = (num >> 24) & 0xff;
        return 5;
    }

    bytes[0] = 0xff;
    bytes[1] = num & 0xff;
    bytes[2] = (num >> 8) & 0xff;
    bytes[3] = (num >> 16) & 0xff;
    bytes[4] = (num >> 24) & 0xff;
    bytes[5] = (num >> 32) & 0xff;
    bytes[6] = (num >> 40) & 0xff;
    bytes[7] = (num >> 48) & 0xff;
    bytes[8] = (num >> 56) & 0xff;
    return 9;
}

/*!
 * \brief serialize a peerMessage into contiguous memory
 * will also calculate checksum and set it
 * currently magic value is hardcoded to bitcoin mainnet, will be fixed in the future
 * make sure that enough bytes are allocated in serialized before calling
 * you can check how many bytes you'll need by doing payloadLength + PEER_MESSAGE_HEADER_LENGTH
 * \param serialized where serialized message will be put. This can be directly sent to a peer
 * \param deserialized message to serialize
 * \sa findPeerMessageLength
 */
void serializePeerMessage(unsigned char *serialized, const peerMessage *deserialized) {
    int off = 0;
    // magic
    // TODO: don't hardcode this
    serializeUInt32(0xd9b4bef9, serialized + off);
    off += 4;
    // command
    int commandLength = strlen(deserialized->command);
    int nullifyLength = 12 - commandLength;
    memcpy(serialized + off, deserialized->command, commandLength);
    off += commandLength;
    memset(serialized + off, 0, nullifyLength);
    off += nullifyLength;
    // payload length
    serializeUInt32(deserialized->payloadLength, serialized + off);
    off += 4;
    // checksum
    unsigned char digestHere[SHA256_DIGEST_LENGTH];
    sha256d(deserialized->payload, deserialized->payloadLength, digestHere);
    memcpy(serialized + off, digestHere, 4);
    off += 4;
    // payload
    memcpy(serialized + off, deserialized->payload, deserialized->payloadLength);
}

/*!
 * \brief deserialize a peer message
 * also verifies that checksum and magic are correct as well as that command string is null-padded on the end
 * currently checks against hardcoded bitcoin mainnet magic, will be fixed in the future
 * \param serialized serialized message (from TCP socket or serializePeerMessage)
 * \param deserialized will be filled out
 * \return whether checsum and magic were ok. If false, it is not guaranteed that deserialized will be fully filled out
 */
bool deserializePeerMessage(const unsigned char *serialized, peerMessage *deserialized) {
    // check magic
    // TODO: don't hardcode this
    if(deserializeUInt32(serialized) != 0xd9b4bef9) {
        return false;
    }
    // payloadLength
    deserialized->payloadLength = deserializeUInt32(serialized + 4 + 12);
    // check checksum
    unsigned char digestHere[SHA256_DIGEST_LENGTH];
    sha256d(serialized + 4 + 12 + 4 + 4, deserialized->payloadLength, digestHere);
    if(memcmp(digestHere, serialized + 4 + 12 + 4, 4)) {
        return false;
    }
    // check command
    // basically, everything after strlen needs to be null, and the last byte must be null (string length < 12)
    if(*(serialized + 4 + 11) != 0) {
        return false;
    }
    for(int i = strlen(serialized + 4); i < 12; ++i) {
        if(*(serialized + 4 + i) != 0) {
            return false;
        }
    }
    memcpy(deserialized->command, serialized + 4, 12);
    // payload
    deserialized->payload = malloc(deserialized->payloadLength);
    memcpy(deserialized->payload, serialized + 4 + 12 + 4 + 4, deserialized->payloadLength);
    return true;
}

/*!
 * \brief determines how long a serialized peer message is based on its serialized form
 * make sure that it is at least PEER_MESSAGE_HEAER_LENGTH before running this, or it might segfault
 * \param serialized peer message in serialized form
 * \return how long it should be
 */
size_t findPeerMessageLength(const unsigned char *serialized) {
    return (size_t)PEER_MESSAGE_HEADER_LENGTH + (size_t)deserializeUInt32(serialized + 4 + 12);
}

/*!
 * \brief deserialize an arbitrary length unsigned integer from little endian bytes
 * \param bytes the bytes to deserialize from
 * \param length how many bytes wide the integer is
 * \return parsed value
 */
unsigned long long deserializeUInt(const unsigned char *bytes, int length) {
    unsigned long long toReturn = 0;
    for(int i = 0; i < length; ++i) {
        toReturn += (bytes[i] << (8 * i));
    }
    return toReturn;
}

/*!
 * \brief serialize an arbitrary length unsigned integer into little endian bytes
 * \param bytes bytes to serialize to
 * \param num integer to serialize
 * \param length how many bytes wide this integer is
 */
void serializeUInt(unsigned char *bytes, unsigned long long num, int length) {
    for(int i = 0; i < length; ++i) {
        bytes[i] = (num >> (8 * i)) & 0xff;
    }
}