/*!
 * \file
 * \brief contains some type definitions for peer related stuff as well as portably including posix socket files
 */

#ifndef LBH_PEERS_H
#define LBH_PEERS_H

#include <stdint.h>
#include <stdbool.h>

#include "types.h"

#ifdef _WIN32
// TODO: Windows support
#include <winsock2.h>
#define CLOSE_SOCKET closesocket
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#define CLOSE_SOCKET close
#define SET_SOCKET_NON_BLOCKING(sockid) fcntl(sockid, F_SETFL, O_NONBLOCK)
#define POLL poll
#endif

#define LBH_MAX_CONNECTIONS 12
#define LBH_MAX_RECVS 6
#define LBH_MAX_SENDS 6

/*!
 * \brief different statuses (statusi?) that a peer can have
 */
typedef enum {
    peerNotConnected, //!< not connected to peer, but peer is not broken or anything either
    peerConnecting, //!< currently connecting to peer
    peerSendingVersion, //!< peer connection complete, sending version and waiting for response
    peerSendingVerack, //!< version received from remote, sending verack
    peerConnected, //!< connection to peer is open, not waiting for response
} peerStatus;

/*!
 * \brief represents a single peer on the P2P network
 */
typedef struct {
    int sockid; //!< socket id
    struct sockaddr_in addr; //!< address and port for this peer
    uint32_t version; //!< the version reported by this peer
    long timeOffset; //!< difference between this node's network time and the local time. Positive means network is ahead
    unsigned char *recvBuffer; //!< stuff which has been received, but not full messages yet
    size_t recvBufferLength; //!< how many bytes of data are in the recvBuffer
    unsigned char *sendBuffer; //!< stuff which is being sent but hasn't yet
    size_t sendBufferLength; //!< how many bytes of data are in sendBuffer
    peerStatus status; //!< the current status of this peer
} peer;

//! pool with multiple peers. Connected includes connecting
typedef struct {
    int peerCount;
    peer *peers;
} peerPool;

void createPeer(uint32_t peerAddr, unsigned int peerPort, peer *newPeer);

/*!
 * \brief function which is run every time the pool loop is run.
 * customState is the custom state passed to the pool when starting it
 * pool is the pool itself
 * should return whether the loop should continue or not
 * if false, peers will be disconnected, peer info written to disk, peer file unlocked, then the pool will be disbanded
 */
typedef bool (*poolLooper)(void *customState, peerPool *pool);

void startPool(stateFn looper);

#endif