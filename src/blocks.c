/*!
 * \file
 * \brief Block header type declaritions and related functions
 *
 * Has functions for accessing the data in a serialized block,
 * calculating block hashes, and verifying blocks
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "crypto.h"
#include "types.h"
#include "blocks.h"

/*!
 * \brief deserialize a block from bytes in memory into a struct.
 *
 * \param serialized a pointer to the serialized block.
 * \param deserialized where the deserialized result will be placed. Make sure there's at least `sizeof(blockHeader)` bytes of space allocated here.
 */
void deserializeHeader(const unsigned char *serialized, blockHeader *deserialized) {
    deserialized->version = deserializeUInt32(serialized);
    memcpy(&(deserialized->previousBlock), serialized + 4, 32);
    memcpy(&(deserialized->merkleRoot), serialized + 4 + 32, 32);
    deserialized->timestamp = deserializeUInt32(serialized + 4 + 32 + 32);
    deserialized->target = deserializeUInt32(serialized + 4 + 32 + 32 + 4);
    deserialized->nonce = deserializeUInt32(serialized + 4 + 32 + 32 + 4 + 4);
    sha256d(serialized, 80, deserialized->hash);
}

/*!
 * \brief serialize a block from struct to raw bytes
 * This function shouldn't really be used, it's mainly for testing and stuff
 *
 * \param deserialized pointer to the struct to be deserialized
 * \param serialized the pointer to where the serialized block will be placed. Allocate at least 80 bytes here.
 */
void serializeHeader(unsigned char *serialized, const blockHeader *deserialized) {
    serializeUInt32(deserialized->version, serialized);
    memcpy(serialized + 4, &(deserialized->previousBlock), 32);
    memcpy(serialized + 4 + 32, &(deserialized->merkleRoot), 32);
    serializeUInt32(deserialized->timestamp, serialized + 4 + 32 + 32);
    serializeUInt32(deserialized->target, serialized + 4 + 32 + 32 + 4);
    serializeUInt32(deserialized->nonce, serialized + 4 + 32 + 32 + 4 + 4);
}

/*!
 * \brief verify that a block header is OK
 * Uses pretty much the same rules as Bitcoin Core.
 * See the headerRejectReason enum for the list of things we might reject for.
 *
 * \param verifyMe the block to be verified (deserialized, in struct form)
 * \param context the block context, or NULL for a genesis block or if you just don't want to verify target/previous hash/timestamp behind
 * \param rejectReason will be set to the reject reason if the function returns false. Can be NULL if you don't care.
 * \return whether the header is valid. If false, reject it!
 * \sa headerRejectReason
 */
bool verifyHeader(const blockHeader *verifyMe, const blockContext *context, headerRejectReason *rejectReason) {
    // CONTEXT VERIFICATION
    if(context != NULL) {
        if(verifyMe->target != context->target) {
            if(rejectReason) *rejectReason = rejectTargetNotEqual;
            return false;
        }
        // memcmp returns non-zero if unequal
        if(memcmp(verifyMe->previousBlock, context->previousBlock, SHA256_DIGEST_LENGTH)) {
            if(rejectReason) *rejectReason = rejectPreviousBlockNotEqual;
            return false;
        }
        if(verifyMe->timestamp <= context->medianTimestamp) {
            if(rejectReason) *rejectReason = rejectTimestampBehind;
            return false;
        }
        if(verifyMe->timestamp > context->networkTimestamp + /* 2 hours */ 2 * 60 * 60) {
            if(rejectReason) *rejectReason = rejectTimestampAhead;
            return false;
        }
    }
    // PoW stuff
    bool PoWOk = true;
    // first byte
    int mainBytesOffset = verifyMe->target >> 24;
    for(int i = SHA256_DIGEST_LENGTH - 1; i >= 0; --i) {
        int curByte = (verifyMe->hash)[i];
        if(i > SHA256_DIGEST_LENGTH - 1 - mainBytesOffset || i <= SHA256_DIGEST_LENGTH - 1 - mainBytesOffset - 3) {
            // before and after main bytes. It only gets to "after" bytes if main are equal.
            if(curByte != 0) { PoWOk = false; break; }
        } else {
            // the main bytes
            int curTargetByte = (verifyMe->target >> (2 - (SHA256_DIGEST_LENGTH - 1 - mainBytesOffset - i)) * 8) & 0xff;
            if(curByte > curTargetByte) { PoWOk = false; break; }
            if(curByte < curTargetByte) break;
        }
    }
    if(!PoWOk) {
        if(rejectReason) *rejectReason = rejectPoW;
        return false;
    }
    // if we've made it here, the block is valid
    return true;
}
