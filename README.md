# libblockheaders, a C library for downloading, verifying, and accessing blockchain headers

For full documentation, including building instructions, please view the [official documentation](https://libblockheaders.markasoftware.com).
