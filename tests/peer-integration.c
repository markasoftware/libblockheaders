/*!
 * \file
 * used to "manually" test peer/network stuff during early development
 * these tests are not really meant to be pass/fail or anything like that.
 * they're just to provide an easy way to play around with peer stuff to get it working.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include "../src/types.h"
#include "../src/peers.h"

// looper function
bool loopy(void *state, peerPool *pool) {
    static int howMany = 0;
    puts("In the loopy thing!");
    printf("State thing: %d\n", *((int *)state));
    printf("How many times looped: %d\n", howMany);
    if(howMany > 50) {
        for(int i = 0; i < pool->peerCount; ++i) {
            printf("Peer status: %d\n", pool->peers[i].status);
        }
        printf("Random number: %d\n", rand());
        puts("Breaking...");
        return false;
    }
    ++howMany;
    return true;
}

int main() {
    srand(time(NULL));
    int answerToEverything = 42;
    stateFn loopyState = {
        .fn = (void (*)())&loopy,
        .state = &answerToEverything,
    };
    startPool(loopyState);
}