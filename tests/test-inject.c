#include <stddef.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

void *__wrap_malloc(int numBytes) {
    return test_malloc(numBytes);
}

void __wrap_free(void *segment) {
    test_free(segment);
}
